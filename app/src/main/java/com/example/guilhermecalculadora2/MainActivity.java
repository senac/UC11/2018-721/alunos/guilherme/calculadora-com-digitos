package com.example.guilhermecalculadora2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private TextView display;
    private Button btnSoma;
    private Button btnSubtracao;
    private Button btnMultiplicacao;
    private Button btnDivisao;
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnPonto;
    private Button btnLimpar;
    private Button btnIgual;
    private final int SOMA = 1;
    private final int SUBTRACAO = 2;
    private final int MULTIPLICACAO = 3;
    private final int DIVISAO = 4;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponentes();

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });





    }

    private void escreverDisplay(View view) {
        Button button = (Button)view;
        String texto = display.getText().toString();

        if (texto.equals())

    }

    private void initComponentes() {
        btnSoma = (Button) findViewById(R.id.btnSoma);
        btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicacao);
        btnDivisao = (Button) findViewById(R.id.btnDivisao);
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnPonto = (Button) findViewById(R.id.btnPonto);
        btnLimpar = (Button) findViewById(R.id.btnLimpar);
        btnIgual = (Button) findViewById(R.id.btnIgual);

        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular(SOMA);


            }
}

    private void calcular(int operacao) {


        }
    }